/* DATA TABLES */

function init_DataTables(results) {

    if( typeof ($.fn.DataTable) === 'undefined'){ return; }
    if (table !== 0) {
        table.destroy();
    }
    var handleDataTableVolume = function() {
        if ($("#datatableVolume").length) {
            table = $("#datatableVolume").DataTable({
                dom: "Blfrtip",
                data: results,
                columns: [
                    {"title":"Date"},
                    {"title":"Periode"},
                    {"title":"poste"},
                    {"title":"Matière"},
                    {"title":"Quantité verre",},
                    {"title":"Quantité defaut"},
                ],
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true
            });
        }
    };
    var handleDataTableRetour = function() {
        if ($("#datatableRetour").length) {
            table = $("#datatableRetour").DataTable({
                dom: "Blfrtip",
                data: results,
                columns: [
                    {"title":"N° commande"},
                    {"title":"N° ligne"},
                    {"title":"N° edition","visible":false},
                    {"title":"Verre D/G"},
                    {"title":"Code défaut"},
                    {"title":"Remarque", "visible":false},
                    {"title":"Date défaut",},
                    {"title":"Heure défaut"},
                    {"title":"Libellé art. commande"},
                    {"title":"Sphère"},
                    {"title":"Cylindre"},
                    {"title":"Axe"},
                    {"title":"Addition"},
                    {"title":"Supplément 1","visible":false},
                    {"title":"Supplément 2","visible":false},
                    {"title":"Supplément 3","visible":false},
                    {"title":"Supplément 4","visible":false},
                    {title:"Supplément 5","visible":false},
                    {title:"Prix commande","visible":false},
                    {title:"Base S-F","visible":false},
                    {title:"Addition S-F","visible":false},
                    {title:"Libellé S-F","visible":false},
                    {title:"Fournisseur S-F","visible":false},
                    {title:"Prix unit. S-F","visible":false},
                    {title:"Provenance","visible":false},
                    {title:"Dernier utilisateu","visible":false},
                    {title:"Num. glanteur","visible":false},
                    {title:"Num. générateur"},
                    {title:"Num. polisseur"},
                    {title:"Num. traitement","visible":false},
                    {title:"Num. vernis","visible":false},
                    {title:"Outil polissage","visible":false},
                    {title:"Type polissage","visible":false},
                    {title:"A polir","visible":false},
                    {title:"A doucir","visible":false},
                    {title:"Contrôle 1","visible":false},
                    {title:"Contrôle 2","visible":false},
                    {title:"Contrôle 3","visible":false},
                    {title:"Num. cyc. trt","visible":false},
                    {title:"Num. cyc. vernis","visible":false},
                    {title:"Heure générateur"}
                ],
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true
            });
        }
    };

    TableManageButtons = function() {
        "use strict";
        return {
            init: function() {
                handleDataTableRetour();
                handleDataTableVolume();
            }
        };
    }();

    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();

        // Get the column API object
        var column = table.column( $(this).attr('data-column') );

        // Toggle the visibility
        column.visible( ! column.visible() );
    } );

    TableManageButtons.init();
}