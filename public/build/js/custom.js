/**
 * Resize function without multiple trigger
 *
 * Usage:
 * $(window).smartresize(function(){
 *     // code here
 * });
 */


(function ($, sr) {
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced() {
            var obj = this, args = arguments;

            function delayed() {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    };

    // smartresize
    jQuery.fn[sr] = function (fn) {
        return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
    };

})(jQuery, 'smartresize');
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE = $('#menu_toggle'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer');


// Sidebar
function init_sidebar() {
// TODO: This is some kind of easy fix, maybe we can improve this
    var setContentHeight = function () {
        // reset height
        $RIGHT_COL.css('min-height', $(window).height());

        var bodyHeight = $BODY.outerHeight(),
            footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
            leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
            contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

        // normalize content
        contentHeight -= $NAV_MENU.height() + footerHeight;

        $RIGHT_COL.css('min-height', contentHeight);
    };

    $SIDEBAR_MENU.find('a').on('click', function (ev) {
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function () {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                $SIDEBAR_MENU.find('li ul').slideUp();
            } else {
                if ($BODY.is(".nav-sm")) {
                    $SIDEBAR_MENU.find("li").removeClass("active active-sm");
                    $SIDEBAR_MENU.find("li ul").slideUp();
                }
            }
            $li.addClass('active');

            $('ul:first', $li).slideDown(function () {
                setContentHeight();
            });
        }
    });

// toggle small or large menu
    $MENU_TOGGLE.on('click', function () {

        if ($BODY.hasClass('nav-md')) {
            $SIDEBAR_MENU.find('li.active ul').hide();
            $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
        } else {
            $SIDEBAR_MENU.find('li.active-sm ul').show();
            $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
        }

        $BODY.toggleClass('nav-md nav-sm');

        setContentHeight();

        $('.dataTable').each(function () {
            $(this).dataTable().fnDraw();
        });
    });

    // check active menu
    $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

    $SIDEBAR_MENU.find('a').filter(function () {
        return this.href == CURRENT_URL;
    }).parent('li').addClass('current-page').parents('ul').slideDown(function () {
        setContentHeight();
    }).parent().addClass('active');

    // recompute content when resizing
    $(window).smartresize(function () {
        setContentHeight();
    });

    setContentHeight();

    // fixed sidebar
    if ($.fn.mCustomScrollbar) {
        $('.menu_fixed').mCustomScrollbar({
            autoHideScrollbar: true,
            theme: 'minimal',
            mouseWheel: {preventDefault: true}
        });
    }
};
// /Sidebar

var randNum = function () {
    return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
};


// Panel toolbox
$(document).ready(function () {
    $('.collapse-link').on('click', function () {
        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function () {
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200);
            $BOX_PANEL.css('height', 'auto');
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });

    $('.close-link').click(function () {
        var $BOX_PANEL = $(this).closest('.x_panel');

        $BOX_PANEL.remove();
    });
});
// /Panel toolbox

// Tooltip
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
});
// /Tooltip

// Progressbar
if ($(".progress .progress-bar")[0]) {
    $('.progress .progress-bar').progressbar();
}
// /Progressbar

// Switchery
$(document).ready(function () {
    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A'
            });
        });
    }
});
// /Switchery


// iCheck
$(document).ready(function () {
    if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
});
// /iCheck

// Table
$('table input').on('ifChecked', function () {
    checkState = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('table input').on('ifUnchecked', function () {
    checkState = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});

var checkState = '';

$('.bulk_action input').on('ifChecked', function () {
    checkState = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('.bulk_action input').on('ifUnchecked', function () {
    checkState = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});
$('.bulk_action input#check-all').on('ifChecked', function () {
    checkState = 'all';
    countChecked();
});
$('.bulk_action input#check-all').on('ifUnchecked', function () {
    checkState = 'none';
    countChecked();
});

function countChecked() {
    if (checkState === 'all') {
        $(".bulk_action input[name='table_records']").iCheck('check');
    }
    if (checkState === 'none') {
        $(".bulk_action input[name='table_records']").iCheck('uncheck');
    }

    var checkCount = $(".bulk_action input[name='table_records']:checked").length;

    if (checkCount) {
        $('.column-title').hide();
        $('.bulk-actions').show();
        $('.action-cnt').html(checkCount + ' Records Selected');
    } else {
        $('.column-title').show();
        $('.bulk-actions').hide();
    }
}


// Accordion
$(document).ready(function () {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
            $expand.text("-");
        } else {
            $expand.text("+");
        }
    });
});

// NProgress
if (typeof NProgress != 'undefined') {
    $(document).ready(function () {
        NProgress.start();
    });

    $(window).load(function () {
        NProgress.done();
    });
}
//hover and retain popover when on popover content
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
        obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
    var container, timeout;

    originalLeave.call(this, obj);

    if (obj.currentTarget) {
        container = $(obj.currentTarget).siblings('.popover');
        timeout = self.timeout;
        container.one('mouseenter', function () {
            //We entered the actual popover – call off the dogs
            clearTimeout(timeout);
            //Let's monitor popover content instead
            container.one('mouseleave', function () {
                $.fn.popover.Constructor.prototype.leave.call(self, self);
            });
        });
    }
};

$('body').popover({
    selector: '[data-popover]',
    trigger: 'click hover',
    delay: {
        show: 50,
        hide: 400
    }
});

function gd(year, month, day) {
    return new Date(year, month - 1, day).getTime();
}


var volumeRetourChart = {}
var volumeFabDailyChart = {}
var volumeFabMonthlyChart = {}
var volumePosteMonthlyChart = {}
var datepicker = $('.reportrange');
var table = 0;

/* DATERANGEPICKER */

function init_daterangepicker() {

    if(!$.fn.daterangepicker){ return; }

    var cb = function(start, end, label) {
        $('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    };

    var optionSet1 = {
        startDate: moment().subtract(1, 'month'),
        endDate: moment(),
        minDate: '01/01/2014',
        maxDate: moment(),
        dateLimit: {
            days: 60
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
/*            'Aujourd\'hui': [moment(), moment()],*/
            'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 derniers jours': [moment().subtract(7, 'days'), moment()],
            '30 derniers jours': [moment().subtract(1, 'month'), moment()],
            'Ce mois': [moment().startOf('month'), moment().endOf('month')],
            'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD/MM/YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Envoyer',
            cancelLabel: 'Annuler',
            fromLabel: 'De',
            toLabel: 'à',
            customRangeLabel: 'Personalisée',
            daysOfWeek: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            firstDay: 1
        }
    };

    $('.reportrange span').html(moment().subtract(1, 'month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    datepicker.daterangepicker(optionSet1, cb);
    datepicker.on('show.daterangepicker', function() {
    });
    datepicker.on('hide.daterangepicker', function() {
    });
    datepicker.on('apply.daterangepicker', function(ev, picker) {
    });
    datepicker.on('cancel.daterangepicker', function(ev, picker) {
    });
    $('#options1').click(function() {
        datepicker.data('daterangepicker').setOptions(optionSet1, cb);
    });
    $('#options2').click(function() {
        datepicker.data('daterangepicker').setOptions(optionSet2, cb);
    });
    $('#destroy').click(function() {
        datepicker.data('daterangepicker').remove();
    });
}

function init_charts() {

    if (datepicker.data('daterangepicker')) {
        var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
        var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
        var startDate1 = datepicker.data('daterangepicker').startDate.format('Y-M-D');
        var endDate1 = datepicker.data('daterangepicker').endDate.format('Y-M-D');
    }
    if (!Chart) {
        return;
    }
    var url = new URL(window.location.href);
    if(url.searchParams.get("datemois")) {
        date = url.searchParams.get("datemois");
        console.log(date);
        datepicker.data('daterangepicker').setStartDate(new Date(date));
        datepicker.data('daterangepicker').setEndDate(moment(new Date(date)).endOf('month'));
        var startDate1 = datepicker.data('daterangepicker').startDate.format('Y-M-D');
        var endDate1 = datepicker.data('daterangepicker').endDate.format('Y-M-D');
    }
    if(url.searchParams.get("datejour")) {
        date = url.searchParams.get("datejour");
        console.log(date);
        datepicker.data('daterangepicker').setStartDate(new Date(date));
        datepicker.data('daterangepicker').setEndDate(new Date(date));
        var startDate1 = datepicker.data('daterangepicker').startDate.format('Y-M-D');
        var endDate1 = datepicker.data('daterangepicker').endDate.format('Y-M-D');

        console.log(startDate1,endDate1);

    }


/*    Chart.defaults.global.legend = {
        enabled: false
    };*/

    if ($('#volumeRetourChart').length ) {
        $.ajax({
            url: '/api/nombredefaut',
            data: ({startDate: startDate, endDate: endDate}),
            success: createChart,
        })
    }

    if ($('#volumeRetourSyntheseChart').length ) {
        $.ajax({
            url: '/api/nombredefaut',
            data: ({startDate: startDate, endDate: endDate}),
            success: createChart,
        })
        $.ajax({
            url: '/api/pourcentagedefaut',
        })
    }

    if ($('#volumeFabDailyChart').length ) {
        $.ajax({
            url: '/api/volumefabdaily',
            data: ({startDate:startDate1, endDate:endDate1}),
            success: createVolumeFabDailyChart,
        })
    }
    if ($('#volumeFabMonthlyChart').length ) {
        $.ajax({
            url: '/api/volumefabmonthly',
            success: createVolumeFabMonthlyChart,
        })
    }
    if ($('#volumePosteMonthlyChart').length ) {
        $.ajax({
            url: '/api/volumepostemonthly',
            data: ({startDate:startDate1, endDate:endDate1}),
            success: createVolumePosteMonthlyChart,
        })
    }
    if ($('#leadtimeFabDailyChart').length ) {
        $.ajax({
            url: '/api/leadtimefabdaily',
            data: ({startDate: startDate, endDate: endDate}),
            success: createLeadtimeFabDailyChart,
        })
    }
    if ($('#leadtimeFabRepartitionChart').length ) {
        $.ajax({
            url: '/api/leadtimefabrepartition',
            data: ({startDate: startDate, endDate: endDate}),
            success: createLeadtimeFabRepartitionChart,
        })
    }
    if ($('#leadtimeFabMonthlyChart').length ) {
        $.ajax({
            url: '/api/leadtimefabmonthly',
            success: createLeadtimeFabMonthlyChart,
        })
    }
}
function createVolumeFabDailyChart(results) {

var chartData =  {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: "380 contrôle 1",
                borderColor: "#fbba00",
                fill: false,
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: []
            },{
                label: "800 contrôle 2",
                borderColor: "grey",
                fill: false,
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: []
            },{
                label: "Objectif contrôle 1",
                borderColor: "red",
                fill: false,
                datalabels: {
                    display: false,
                },
                data: []
            }]
        },
        options: {
            plugins: {
                datalabels: {
                    backgroundColor: function(context) {
                        return context.dataset.backgroundColor;
                    },
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        max : 3000,
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false
                    }
                }]
            },
            layout: {
                padding: {
                    right: 15,
                }
            }
        }
    };
    var ctx = $('#volumeFabDailyChart')[0];
    volumeFabDailyChart = new Chart(ctx,chartData);

    //var labels = [], data = [];
    for (var line of results) {
        chartData.data.labels.push(line.DATE);
        chartData.data.datasets[0].data.push(line.VOLUME380);
        chartData.data.datasets[1].data.push(line.VOLUME800);
        chartData.data.datasets[2].data.push(1100);
    }
    volumeFabDailyChart.update();

    ctx.onclick = function (evt) {

        if ($('#synthese').length) {
            var activePoint = volumeFabDailyChart.lastActive[0];
            if (activePoint !== undefined) {
                var index = activePoint._index;
                var datejour = chartData.data.labels[index];
                window.location.replace('/volumefabanalyse?datejour=' + datejour + '#volumeposte');
            }
        }

        if ($('#analyse').length) {

        var activePoint = volumeFabDailyChart.lastActive[0];
        if (activePoint !== undefined) {
            var index = activePoint._index;
            var date = chartData.data.labels[index];
            datepicker.data('daterangepicker').setStartDate(new Date(date));
            datepicker.data('daterangepicker').setEndDate(new Date(date));
            var startDate = datepicker.data('daterangepicker').startDate.format('Y-M-D');
            var endDate = datepicker.data('daterangepicker').endDate.format('Y-M-D');

                removeData(volumePosteMonthlyChart);
                $.ajax({
                    url: '/api/volumepostemonthly',
                    data: ({startDate:startDate, endDate:endDate}),
                    success: createVolumePosteMonthlyChart,
                });
                $.ajax({
                    url: '/api/volumedetail',
                    data: ({startDate:startDate, endDate:endDate}),
                    success: init_DataTables,
                });
            }
        }

    };
}

function createVolumeFabMonthlyChart(results) {

    var labels = [], data0 = [], data1 = [];
    for (var line of results) {
        labels.push(line.DATE);
        data0.push(line.VOLUME380);
        data1.push(line.VOLUME800);
    }
    var ctx = $('#volumeFabMonthlyChart')
    volumeFabMonthlyChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: "380 contrôle 1",
                backgroundColor: "grey",
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: data0

            },{
                label: "800 contrôle 2",
                backgroundColor: "#fbba00",
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: data1
            }]
        },
        options: {
            onClick: graphClickEvent,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function createVolumePosteMonthlyChart(results) {

    var labels = [], data = [];
    for (var line of results) {
        labels.push(line.SFPST);
        data.push(line.SUMQVE);
    }
    var ctx = $('#volumePosteMonthlyChart')
    volumePosteMonthlyChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: "Volume",
                backgroundColor: "#fbba00",
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: data
            }]
        },
        options: {
            onClick: graphClickEvent,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        }
    });
}

function createLeadtimeFabDailyChart(results) {

    var chartData = {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: "Leadtime jour",
                borderColor: "#fbba00",
                fill: false,
                datalabels: {
                    align: 'start',
                    anchor: 'end',
                },
                data: []
            },{
                label: "Objectif",
                borderColor: "red",
                fill: false,
                datalabels: {
                    display: false,
                },
                data: []
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepValue: 5,
                        max: 100
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false
                    }
                }]
            }
        }
    };

    var ctx = $('#leadtimeFabDailyChart')[0];
    leadtimeFabDailyChart = new Chart(ctx,chartData);

    //var labels = [], data = [];
    for (var line of results) {
        chartData.data.labels.push(line.date);
        chartData.data.datasets[0].data.push(line.pourcentage);
        chartData.data.datasets[1].data.push(90);
    }
    leadtimeFabDailyChart.update();


    ctx.onclick = function (evt) {

    if ($('#synthese').length) {

        var activePoint = leadtimeFabDailyChart.lastActive[0];
        if (activePoint !== undefined) {
            var index = activePoint._index;
            var dateJour = chartData.data.labels[index];
            var formattedDateJour = dateJour.slice(0, 4) + "-" + dateJour.slice(4, 6) + "-" + dateJour.slice(6, 8);
            window.location.replace('/leadtimefabanalyse?datejour=' + formattedDateJour);
        }
    }

    if ($('#analyse').length) {

            var activePoint = leadtimeFabDailyChart.lastActive[0];
            if (activePoint !== undefined) {
                var index = activePoint._index;
                var date = chartData.data.labels[index];
                var formattedDate = date.slice(0, 4) + "-" + date.slice(4, 6) + "-" + date.slice(6, 8);
                datepicker.data('daterangepicker').setStartDate(new Date(formattedDate));
                datepicker.data('daterangepicker').setEndDate(new Date(formattedDate));
                var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
                var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
                if ($('#analyse').length) {

                    removeData(leadtimeFabRepartitionChart);
                    $.ajax({
                        url: '/api/leadtimefabrepartition',
                        data: ({startDate: startDate, endDate: endDate}),
                        success: createLeadtimeFabRepartitionChart,
                    });
                }
            }

        }
    };
}

function createLeadtimeFabRepartitionChart(results) {

    var labels = [], data = [];
    for (var line of results) {
        labels.push(line.date);
        data.push(line.pourcentage);
    }
    var ctx = $('#leadtimeFabRepartitionChart')
    leadtimeFabRepartitionChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: "Repartition leadtime",
                backgroundColor: "#fbba00",
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: data
            }]
        },
        options: {
            onClick: graphClickEvent,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepValue: 5,
                        max: 100
                    }
                }]
            }
        }
    });
}

function createLeadtimeFabMonthlyChart(results) {

    var chartData = {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: "Leadtime jour",
                borderColor: "#fbba00",
                fill: false,
                datalabels: {
                    align: 'start',
                    anchor: 'end',
                },
                data: []
            },{
                label: "Objectif",
                borderColor: "red",
                fill: false,
                datalabels: {
                    display: false,
                },
                data: []
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepValue: 5,
                        max: 100
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false
                    }
                }]
            }
        }
    };

    var ctx = $('#leadtimeFabMonthlyChart')[0];
    leadtimeFabMonthlyChart = new Chart(ctx,chartData);

    //var labels = [], data = [];
    for (var line of results) {
        chartData.data.labels.push(line.date);
        chartData.data.datasets[0].data.push(line.pourcentage);
        chartData.data.datasets[1].data.push(90);
    }
    leadtimeFabMonthlyChart.update();

    ctx.onclick = function (evt) {
        var activePoint = leadtimeFabMonthlyChart.lastActive[0];
        if (activePoint !== undefined) {
            var index = activePoint._index;
            var date = chartData.data.labels[index];
            var formattedDate = date.slice(0, 4) + "-" + date.slice(4, 6);
            datepicker.data('daterangepicker').setStartDate(new Date(formattedDate));
            datepicker.data('daterangepicker').setEndDate(moment(new Date(formattedDate)).endOf('month'));
            var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
            var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
            if ($('#analyse').length) {
                removeData(leadtimeFabDailyChart);
                removeData(leadtimeFabRepartitionChart);
                $.ajax({
                    url: '/api/leadtimefabdaily',
                    data: ({startDate:startDate, endDate:endDate}),
                    success: createLeadtimeFabDailyChart,
                });
                $.ajax({
                    url: '/api/leadtimefabrepartition',
                    data: ({startDate:startDate, endDate:endDate}),
                    success: createLeadtimeFabRepartitionChart,
                });
            }
        }

    };
}

function createChart (results) {

    var labels = [], data = [];

    if ($('#volumeRetourChart').length ) {
        for (var line of results) {
            labels.push(line.RFDEFAUT);
            data.push(line.NBDEFAUT);
        }
        var ctx = $('#volumeRetourChart')
        volumeRetourChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: "Nombre de retour",
                    backgroundColor: "#fbba00",
                    data: data
                }]
            },
            options: {
                onClick: graphClickEvent,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }
}

function graphClickEvent(event, array){
    console.log(array[0]._chart.canvas.id);
    console.log($('#analyse').length);
    console.log($('#synthese').length);
    console.log(array[0]);

    if ($('#synthese').length) {
        if (array[0]._chart.canvas.id == 'volumePosteMonthlyChart') {
            var poste = array[0]._model.label.split(" ")[0];
            window.location.replace('/volumefabanalyse?poste='+poste + '#detail');
        }
        if (array[0]._chart.canvas.id == 'volumeFabMonthlyChart') {
            var datemois = array[0]._model.label;
            window.location.replace('/volumefabanalyse?datemois='+datemois + '#volumejournalier');
        }
    }

    if ($('#analyse').length) {
        if (array[0]._chart.canvas.id == 'volumePosteMonthlyChart') {
            var startDate = datepicker.data('daterangepicker').startDate.format('Y-M-D');
            var endDate = datepicker.data('daterangepicker').endDate.format('Y-M-D');
            var poste = array[0]._model.label;
            $.ajax({
                url: '/api/volumedetail',
                data: ({startDate: startDate, endDate: endDate,poste: poste}),
                success:init_DataTables,
            });
        }

        if (array[0]._chart.canvas.id == 'volumeFabMonthlyChart') {

            var date = array[0]._model.label;
            datepicker.data('daterangepicker').setStartDate(new Date(date));
            datepicker.data('daterangepicker').setEndDate(moment(new Date(date)).endOf('month'));
            var startDate = datepicker.data('daterangepicker').startDate.format('Y-M-D');
            var endDate = datepicker.data('daterangepicker').endDate.format('Y-M-D');
            removeData(volumeFabDailyChart);
            removeData(volumePosteMonthlyChart);
            $.ajax({
                url: '/api/volumefabdaily',
                data: ({startDate: startDate, endDate: endDate}),
                success:createVolumeFabDailyChart,
            })
            $.ajax({
                url: '/api/volumepostemonthly',
                data: ({startDate: startDate, endDate: endDate}),
                success:createVolumePosteMonthlyChart,
                });
            $.ajax({
                url: '/api/volumedetail',
                data: ({startDate: startDate, endDate: endDate}),
                success:init_DataTables,
            });
        }

        if (array[0]._chart.canvas.id == 'volumeRetourChart') {
            var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
            var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
            var defaut = array[0]._model.label;

            $.ajax({
                url: '/api/detaildefaut',
                data: ({startDate: startDate, endDate: endDate, defaut: defaut}),
                success: init_DataTables
            });
        }
    }
}

function init_table(){
    if ($("#datatableVolume").length) {
        var url = new URL(window.location.href);
        if(url.searchParams.get("poste")) {
            var poste = url.searchParams.get("poste");
            console.log(poste);
        }
        var startDate = datepicker.data('daterangepicker').startDate.format('Y-M-D');
        var endDate = datepicker.data('daterangepicker').endDate.format('Y-M-D');
        $.ajax({
            url: '/api/volumedetail',
            data: ({startDate: startDate, endDate: endDate,poste: poste}),
            success: init_DataTables
        });
    }

    if ($("#datatableRetour").length) {
        var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
        var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
        $.ajax({
            url: '/api/detaildefaut',
            data: ({startDate: startDate, endDate: endDate}),
            success: init_DataTables
        });
    }
}

function addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}
function removeData(chart) {
    chart.destroy();
}
/* DATA TABLES */

function init_DataTables(results) {

    if( typeof ($.fn.DataTable) === 'undefined'){ return; }
    if (table !== 0) {
        table.destroy();
    }
    var handleDataTableVolume = function() {
        if ($("#datatableVolume").length) {
            table = $("#datatableVolume").DataTable({
                dom: "Blfrtip",
                data: results,
                columns: [
                    {"title":"Date"},
                    {"title":"Periode"},
                    {"title":"poste"},
                    {"title":"Matière"},
                    {"title":"Quantité verre",},
                    {"title":"Quantité defaut"},
                ],
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true
            });
        }
    };
    var handleDataTableRetour = function() {
        if ($("#datatableRetour").length) {
            table = $("#datatableRetour").DataTable({
                dom: "Blfrtip",
                data: results,
                columns: [
                    {"title":"N° commande"},
                    {"title":"N° ligne"},
                    {"title":"N° edition","visible":false},
                    {"title":"Verre D/G"},
                    {"title":"Code défaut"},
                    {"title":"Remarque", "visible":false},
                    {"title":"Date défaut",},
                    {"title":"Heure défaut"},
                    {"title":"Libellé art. commande"},
                    {"title":"Sphère"},
                    {"title":"Cylindre"},
                    {"title":"Axe"},
                    {"title":"Addition"},
                    {"title":"Supplément 1","visible":false},
                    {"title":"Supplément 2","visible":false},
                    {"title":"Supplément 3","visible":false},
                    {"title":"Supplément 4","visible":false},
                    {title:"Supplément 5","visible":false},
                    {title:"Prix commande","visible":false},
                    {title:"Base S-F","visible":false},
                    {title:"Addition S-F","visible":false},
                    {title:"Libellé S-F","visible":false},
                    {title:"Fournisseur S-F","visible":false},
                    {title:"Prix unit. S-F","visible":false},
                    {title:"Provenance","visible":false},
                    {title:"Dernier utilisateu","visible":false},
                    {title:"Num. glanteur","visible":false},
                    {title:"Num. générateur"},
                    {title:"Num. polisseur"},
                    {title:"Num. traitement","visible":false},
                    {title:"Num. vernis","visible":false},
                    {title:"Outil polissage","visible":false},
                    {title:"Type polissage","visible":false},
                    {title:"A polir","visible":false},
                    {title:"A doucir","visible":false},
                    {title:"Contrôle 1","visible":false},
                    {title:"Contrôle 2","visible":false},
                    {title:"Contrôle 3","visible":false},
                    {title:"Num. cyc. trt","visible":false},
                    {title:"Num. cyc. vernis","visible":false},
                    {title:"Heure générateur"}
                ],
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true
            });
        }
    };

    TableManageButtons = function() {
        "use strict";
        return {
            init: function() {
                handleDataTableRetour();
                handleDataTableVolume();
            }
        };
    }();

    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();

        // Get the column API object
        var column = table.column( $(this).attr('data-column') );

        // Toggle the visibility
        column.visible( ! column.visible() );
    } );

    TableManageButtons.init();
}

function init_multiselect() {

    $('#selectMatiere').multiselect({
        maxHeight: 200,
        enableFiltering: true,
        includeSelectAllOption: true,
        selectAllText: 'Selectionner tout',
        allSelectedText: 'All Selected'
    });
    $('#selectMatiere').multiselect('selectAll', false);
    $('#selectMatiere').multiselect('updateButtonText');

    $('#selectDefaut').multiselect({
        maxHeight: 200,
        enableFiltering: true,
        includeSelectAllOption: true,
        selectAllText: 'Selectionner tout',
        allSelectedText: 'All Selected'
    });
    $('#selectDefaut').multiselect('selectAll', false);
    $('#selectDefaut').multiselect('updateButtonText');
}

function clickButton() {

    $('#test').click(function() {
        screenshot();
    });

    $('#validerLeadtime').click(function() {
        var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
        var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
        removeData(leadtimeFabDailyChart);
        removeData(leadtimeFabRepartitionChart);
        $.ajax({
            url: '/api/leadtimefabdaily',
            data: ({startDate: startDate, endDate: endDate}),
            success:createLeadtimeFabDailyChart,
        })
        $.ajax({
            url: '/api/leadtimefabrepartition',
            data: ({startDate: startDate, endDate: endDate}),
            success:createLeadtimeFabRepartitionChart,
        });
    });

    $('#validerVolume').click(function() {
        var startDate = datepicker.data('daterangepicker').startDate.format('Y-M-D');
        var endDate = datepicker.data('daterangepicker').endDate.format('Y-M-D');
        removeData(volumeFabDailyChart);
        removeData(volumePosteMonthlyChart);
        $.ajax({
            url: '/api/volumefabdaily',
            data: ({startDate: startDate, endDate: endDate}),
            success:createVolumeFabDailyChart,
        })
        $.ajax({
            url: '/api/volumepostemonthly',
            data: ({startDate: startDate, endDate: endDate}),
            success:createVolumePosteMonthlyChart,
        });
        $.ajax({
            url: '/api/volumedetail',
            data: ({startDate: startDate, endDate: endDate}),
            success:init_DataTables,
        });
    });
    $('#validerRetour').click(function() {

        var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
        var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
        var defautRaw = $('#selectDefaut option:selected');

        var defaut = [];
        for (var i = 0 ; i < defautRaw.length ; i++) {
            defaut.push(defautRaw[i].value);
        }

        console.log(defaut);

        removeData(volumeRetourChart);
        $.ajax({
            url: '/api/nombredefaut',
            data: ({startDate:startDate, endDate:endDate, defaut:defaut}),
            success:createChart,
        }).done(
            $.ajax({
                url: '/api/detaildefaut',
                data: ({startDate:startDate, endDate:endDate, defaut:defaut}),
                success:init_DataTables,
            }));
    });
}

function screenshot(){
    html2canvas(document.getElementById('capture'),{
        allowTaint: true, scale: 2,
    })
        .then(function(canvas) {
            var link = document.createElement('a');
            link.href = canvas.toDataURL('image/png');
            var newData = link.href.replace(/^data:image\/png/, "data:application/octet-stream");
            $("#btn-Convert-Html2Image").attr("download", "indicateur.png").attr("href", newData);

        // document.body.appendChild(canvas);
    });
}

var $loading = $('#loadingDiv').hide();
$(document)
    .ajaxStart(function () {
        $loading.show();
    })
    .ajaxStop(function () {
        $loading.hide();
        if(window.location.hash) {
            $('html,body').animate({
                scrollTop: '+=' + $(window.location.hash).offset().top + 'px'
            }, 'slow');
        }
    });

$(document).ready(function () {
    init_sidebar();
    init_daterangepicker();
    init_charts();
    init_table();
    init_multiselect();
    clickButton();
});



