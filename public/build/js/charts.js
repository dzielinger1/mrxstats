function init_charts() {

    if (datepicker.data('daterangepicker')) {
        var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
        var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
        var startDate1 = datepicker.data('daterangepicker').startDate.format('Y-M-D');
        var endDate1 = datepicker.data('daterangepicker').endDate.format('Y-M-D');
    }
    if (!Chart) {
        return;
    }
    var url = new URL(window.location.href);
    if(url.searchParams.get("datemois")) {
        date = url.searchParams.get("datemois");
        console.log(date);
        datepicker.data('daterangepicker').setStartDate(new Date(date));
        datepicker.data('daterangepicker').setEndDate(moment(new Date(date)).endOf('month'));
        var startDate1 = datepicker.data('daterangepicker').startDate.format('Y-M-D');
        var endDate1 = datepicker.data('daterangepicker').endDate.format('Y-M-D');
    }
    if(url.searchParams.get("datejour")) {
        date = url.searchParams.get("datejour");
        console.log(date);
        datepicker.data('daterangepicker').setStartDate(new Date(date));
        datepicker.data('daterangepicker').setEndDate(new Date(date));
        var startDate1 = datepicker.data('daterangepicker').startDate.format('Y-M-D');
        var endDate1 = datepicker.data('daterangepicker').endDate.format('Y-M-D');

        console.log(startDate1,endDate1);

    }


    /*    Chart.defaults.global.legend = {
            enabled: false
        };*/

    if ($('#volumeRetourChart').length ) {
        $.ajax({
            url: '/api/nombredefaut',
            data: ({startDate: startDate, endDate: endDate}),
            success: createChart,
        })
    }

    if ($('#volumeRetourSyntheseChart').length ) {
        $.ajax({
            url: '/api/nombredefaut',
            data: ({startDate: startDate, endDate: endDate}),
            success: createChart,
        })
        $.ajax({
            url: '/api/pourcentagedefaut',
        })
    }

    if ($('#volumeFabDailyChart').length ) {
        $.ajax({
            url: '/api/volumefabdaily',
            data: ({startDate:startDate1, endDate:endDate1}),
            success: createVolumeFabDailyChart,
        })
    }
    if ($('#volumeFabMonthlyChart').length ) {
        $.ajax({
            url: '/api/volumefabmonthly',
            success: createVolumeFabMonthlyChart,
        })
    }
    if ($('#volumePosteMonthlyChart').length ) {
        $.ajax({
            url: '/api/volumepostemonthly',
            data: ({startDate:startDate1, endDate:endDate1}),
            success: createVolumePosteMonthlyChart,
        })
    }
    if ($('#leadtimeFabDailyChart').length ) {
        $.ajax({
            url: '/api/leadtimefabdaily',
            data: ({startDate: startDate, endDate: endDate}),
            success: createLeadtimeFabDailyChart,
        })
    }
    if ($('#leadtimeFabRepartitionChart').length ) {
        $.ajax({
            url: '/api/leadtimefabrepartition',
            data: ({startDate: startDate, endDate: endDate}),
            success: createLeadtimeFabRepartitionChart,
        })
    }
    if ($('#leadtimeFabMonthlyChart').length ) {
        $.ajax({
            url: '/api/leadtimefabmonthly',
            success: createLeadtimeFabMonthlyChart,
        })
    }
}

function createVolumeFabDailyChart(results) {

    var chartData =  {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: "380 contrôle 1",
                borderColor: "#fbba00",
                fill: false,
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: []
            },{
                label: "800 contrôle 2",
                borderColor: "grey",
                fill: false,
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: []
            },{
                label: "Objectif contrôle 1",
                borderColor: "red",
                fill: false,
                datalabels: {
                    display: false,
                },
                data: []
            }]
        },
        options: {
            plugins: {
                datalabels: {
                    backgroundColor: function(context) {
                        return context.dataset.backgroundColor;
                    },
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        max : 3000,
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false
                    }
                }]
            },
            layout: {
                padding: {
                    right: 15,
                }
            }
        }
    };
    var ctx = $('#volumeFabDailyChart')[0];
    volumeFabDailyChart = new Chart(ctx,chartData);

    //var labels = [], data = [];
    for (var line of results) {
        chartData.data.labels.push(line.DATE);
        chartData.data.datasets[0].data.push(line.VOLUME380);
        chartData.data.datasets[1].data.push(line.VOLUME800);
        chartData.data.datasets[2].data.push(1100);
    }
    volumeFabDailyChart.update();

    ctx.onclick = function (evt) {

        if ($('#synthese').length) {
            var activePoint = volumeFabDailyChart.lastActive[0];
            if (activePoint !== undefined) {
                var index = activePoint._index;
                var datejour = chartData.data.labels[index];
                window.location.replace('/volumefabanalyse?datejour=' + datejour + '#volumeposte');
            }
        }

        if ($('#analyse').length) {

            var activePoint = volumeFabDailyChart.lastActive[0];
            if (activePoint !== undefined) {
                var index = activePoint._index;
                var date = chartData.data.labels[index];
                datepicker.data('daterangepicker').setStartDate(new Date(date));
                datepicker.data('daterangepicker').setEndDate(new Date(date));
                var startDate = datepicker.data('daterangepicker').startDate.format('Y-M-D');
                var endDate = datepicker.data('daterangepicker').endDate.format('Y-M-D');

                removeData(volumePosteMonthlyChart);
                $.ajax({
                    url: '/api/volumepostemonthly',
                    data: ({startDate:startDate, endDate:endDate}),
                    success: createVolumePosteMonthlyChart,
                });
                $.ajax({
                    url: '/api/volumedetail',
                    data: ({startDate:startDate, endDate:endDate}),
                    success: init_DataTables,
                });
            }
        }

    };
}

function createVolumeFabMonthlyChart(results) {

    var labels = [], data0 = [], data1 = [];
    for (var line of results) {
        labels.push(line.DATE);
        data0.push(line.VOLUME380);
        data1.push(line.VOLUME800);
    }
    var ctx = $('#volumeFabMonthlyChart')
    volumeFabMonthlyChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: "380 contrôle 1",
                backgroundColor: "grey",
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: data0

            },{
                label: "800 contrôle 2",
                backgroundColor: "#fbba00",
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: data1
            }]
        },
        options: {
            onClick: graphClickEvent,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function createVolumePosteMonthlyChart(results) {

    var labels = [], data = [];
    for (var line of results) {
        labels.push(line.SFPST);
        data.push(line.SUMQVE);
    }
    var ctx = $('#volumePosteMonthlyChart')
    volumePosteMonthlyChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: "Volume",
                backgroundColor: "#fbba00",
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: data
            }]
        },
        options: {
            onClick: graphClickEvent,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        }
    });
}

function createLeadtimeFabDailyChart(results) {

    var chartData = {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: "Leadtime jour",
                borderColor: "#fbba00",
                fill: false,
                datalabels: {
                    align: 'start',
                    anchor: 'end',
                },
                data: []
            },{
                label: "Objectif",
                borderColor: "red",
                fill: false,
                datalabels: {
                    display: false,
                },
                data: []
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepValue: 5,
                        max: 100
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false
                    }
                }]
            }
        }
    };

    var ctx = $('#leadtimeFabDailyChart')[0];
    leadtimeFabDailyChart = new Chart(ctx,chartData);

    //var labels = [], data = [];
    for (var line of results) {
        chartData.data.labels.push(line.date);
        chartData.data.datasets[0].data.push(line.pourcentage);
        chartData.data.datasets[1].data.push(90);
    }
    leadtimeFabDailyChart.update();


    ctx.onclick = function (evt) {

        if ($('#synthese').length) {

            var activePoint = leadtimeFabDailyChart.lastActive[0];
            if (activePoint !== undefined) {
                var index = activePoint._index;
                var dateJour = chartData.data.labels[index];
                var formattedDateJour = dateJour.slice(0, 4) + "-" + dateJour.slice(4, 6) + "-" + dateJour.slice(6, 8);
                window.location.replace('/leadtimefabanalyse?datejour=' + formattedDateJour);
            }
        }

        if ($('#analyse').length) {

            var activePoint = leadtimeFabDailyChart.lastActive[0];
            if (activePoint !== undefined) {
                var index = activePoint._index;
                var date = chartData.data.labels[index];
                var formattedDate = date.slice(0, 4) + "-" + date.slice(4, 6) + "-" + date.slice(6, 8);
                datepicker.data('daterangepicker').setStartDate(new Date(formattedDate));
                datepicker.data('daterangepicker').setEndDate(new Date(formattedDate));
                var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
                var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
                if ($('#analyse').length) {

                    removeData(leadtimeFabRepartitionChart);
                    $.ajax({
                        url: '/api/leadtimefabrepartition',
                        data: ({startDate: startDate, endDate: endDate}),
                        success: createLeadtimeFabRepartitionChart,
                    });
                }
            }

        }
    };
}

function createLeadtimeFabRepartitionChart(results) {

    var labels = [], data = [];
    for (var line of results) {
        labels.push(line.date);
        data.push(line.pourcentage);
    }
    var ctx = $('#leadtimeFabRepartitionChart')
    leadtimeFabRepartitionChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: "Repartition leadtime",
                backgroundColor: "#fbba00",
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                },
                data: data
            }]
        },
        options: {
            onClick: graphClickEvent,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepValue: 5,
                        max: 100
                    }
                }]
            }
        }
    });
}

function createLeadtimeFabMonthlyChart(results) {

    var chartData = {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: "Leadtime jour",
                borderColor: "#fbba00",
                fill: false,
                datalabels: {
                    align: 'start',
                    anchor: 'end',
                },
                data: []
            },{
                label: "Objectif",
                borderColor: "red",
                fill: false,
                datalabels: {
                    display: false,
                },
                data: []
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepValue: 5,
                        max: 100
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false
                    }
                }]
            }
        }
    };

    var ctx = $('#leadtimeFabMonthlyChart')[0];
    leadtimeFabMonthlyChart = new Chart(ctx,chartData);

    //var labels = [], data = [];
    for (var line of results) {
        chartData.data.labels.push(line.date);
        chartData.data.datasets[0].data.push(line.pourcentage);
        chartData.data.datasets[1].data.push(90);
    }
    leadtimeFabMonthlyChart.update();

    ctx.onclick = function (evt) {
        var activePoint = leadtimeFabMonthlyChart.lastActive[0];
        if (activePoint !== undefined) {
            var index = activePoint._index;
            var date = chartData.data.labels[index];
            var formattedDate = date.slice(0, 4) + "-" + date.slice(4, 6);
            datepicker.data('daterangepicker').setStartDate(new Date(formattedDate));
            datepicker.data('daterangepicker').setEndDate(moment(new Date(formattedDate)).endOf('month'));
            var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
            var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
            if ($('#analyse').length) {
                removeData(leadtimeFabDailyChart);
                removeData(leadtimeFabRepartitionChart);
                $.ajax({
                    url: '/api/leadtimefabdaily',
                    data: ({startDate:startDate, endDate:endDate}),
                    success: createLeadtimeFabDailyChart,
                });
                $.ajax({
                    url: '/api/leadtimefabrepartition',
                    data: ({startDate:startDate, endDate:endDate}),
                    success: createLeadtimeFabRepartitionChart,
                });
            }
        }

    };
}

function createChart (results) {

    var labels = [], data = [];

    if ($('#volumeRetourChart').length ) {
        for (var line of results) {
            labels.push(line.RFDEFAUT);
            data.push(line.NBDEFAUT);
        }
        var ctx = $('#volumeRetourChart')
        volumeRetourChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: "Nombre de retour",
                    backgroundColor: "#fbba00",
                    data: data
                }]
            },
            options: {
                onClick: graphClickEvent,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }
}

function graphClickEvent(event, array){
    console.log(array[0]._chart.canvas.id);
    console.log($('#analyse').length);
    console.log($('#synthese').length);
    console.log(array[0]);

    if ($('#synthese').length) {
        if (array[0]._chart.canvas.id == 'volumePosteMonthlyChart') {
            var poste = array[0]._model.label.split(" ")[0];
            window.location.replace('/volumefabanalyse?poste='+poste + '#detail');
        }
        if (array[0]._chart.canvas.id == 'volumeFabMonthlyChart') {
            var datemois = array[0]._model.label;
            window.location.replace('/volumefabanalyse?datemois='+datemois + '#volumejournalier');
        }
    }

    if ($('#analyse').length) {
        if (array[0]._chart.canvas.id == 'volumePosteMonthlyChart') {
            var startDate = datepicker.data('daterangepicker').startDate.format('Y-M-D');
            var endDate = datepicker.data('daterangepicker').endDate.format('Y-M-D');
            var poste = array[0]._model.label;
            $.ajax({
                url: '/api/volumedetail',
                data: ({startDate: startDate, endDate: endDate,poste: poste}),
                success:init_DataTables,
            });
        }

        if (array[0]._chart.canvas.id == 'volumeFabMonthlyChart') {

            var date = array[0]._model.label;
            datepicker.data('daterangepicker').setStartDate(new Date(date));
            datepicker.data('daterangepicker').setEndDate(moment(new Date(date)).endOf('month'));
            var startDate = datepicker.data('daterangepicker').startDate.format('Y-M-D');
            var endDate = datepicker.data('daterangepicker').endDate.format('Y-M-D');
            removeData(volumeFabDailyChart);
            removeData(volumePosteMonthlyChart);
            $.ajax({
                url: '/api/volumefabdaily',
                data: ({startDate: startDate, endDate: endDate}),
                success:createVolumeFabDailyChart,
            })
            $.ajax({
                url: '/api/volumepostemonthly',
                data: ({startDate: startDate, endDate: endDate}),
                success:createVolumePosteMonthlyChart,
            });
            $.ajax({
                url: '/api/volumedetail',
                data: ({startDate: startDate, endDate: endDate}),
                success:init_DataTables,
            });
        }

        if (array[0]._chart.canvas.id == 'volumeRetourChart') {
            var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
            var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
            var defaut = array[0]._model.label;

            $.ajax({
                url: '/api/detaildefaut',
                data: ({startDate: startDate, endDate: endDate, defaut: defaut}),
                success: init_DataTables
            });
        }
    }
}

function init_table(){
    if ($("#datatableVolume").length) {
        var url = new URL(window.location.href);
        if(url.searchParams.get("poste")) {
            var poste = url.searchParams.get("poste");
            console.log(poste);
        }
        var startDate = datepicker.data('daterangepicker').startDate.format('Y-M-D');
        var endDate = datepicker.data('daterangepicker').endDate.format('Y-M-D');
        $.ajax({
            url: '/api/volumedetail',
            data: ({startDate: startDate, endDate: endDate,poste: poste}),
            success: init_DataTables
        });
    }

    if ($("#datatableRetour").length) {
        var startDate = datepicker.data('daterangepicker').startDate.format('YMMDD');
        var endDate = datepicker.data('daterangepicker').endDate.format('YMMDD');
        $.ajax({
            url: '/api/detaildefaut',
            data: ({startDate: startDate, endDate: endDate}),
            success: init_DataTables
        });
    }
}

function addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}
function removeData(chart) {
    chart.destroy();
}