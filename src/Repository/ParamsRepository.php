<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 01/03/2018
 * Time: 09:36
 */

namespace App\Repository;


class ParamsRepository extends Repository
{
    public function getCodeArticle(){
        $query = $this->db->prepare("SELECT CACOD, TRIM(CALIB)
        FROM XMRFIC.CODART01
        ORDER BY CACOD ASC");

        $query->execute();

        $result = $query->fetchAll();

        return $result;
    }

    public function getCodeDefaut(){
        $query = $this->db->prepare("SELECT DFCOD, TRIM(DFLIB) 
        FROM XMRFIC.DEFFABL1
        ORDER BY DFLIB ASC");
        $query->execute();
        $result = $query->fetchAll();
        return $result;
    }
}