<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 08/01/2018
 * Time: 14:26
 */

namespace App\Repository;

class VolumeRepository extends Repository
{

    public function getVolumePosteMonthly()
    {
        $query = $this->db->prepare("SELECT * FROM \"STATS\".\"STATFAB00\"
where upper(sfmat) <> 'MINÉRAL'
and upper(sfmat) <> 'MINERAL'
and left(sfpst,3) <> '999' --global
and sfdat >= date(now()) - 1 month");

        $query->execute();

        return $query->fetchAll(\PDO::FETCH_OBJ);

    }

    public function getVolumeDaily($date)
    {
        $query = $this->db->prepare("SELECT SUM (sfqve) AS sumqve, sfdat, sfpst FROM \"STATS\".\"STATFAB00\"
where upper(sfmat) <> 'MINÉRAL'
and upper(sfmat) <> 'MINERAL'
and sfdat >= :date
and left(sfpst,3) in ('380','800')
GROUP BY sfpst, sfdat
ORDER BY sfdat DESC");

        $query->bindParam(':date', $date);

        $query->execute();

        return $query->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getVolumeMonthly($date)
    {
        $query = $this->db->prepare("SELECT SUM (sfqve) AS sumqve,YEAR (sfdat) AS annee, MONTH (sfdat) AS mois, sfpst FROM \"STATS\".\"STATFAB00\"
where upper(sfmat) != 'MINÉRAL'
and upper(sfmat) != 'MINERAL'
and sfdat >= :date
and left(sfpst,3) in ('380','800')
GROUP BY sfpst, YEAR (sfdat), MONTH (sfdat)
ORDER BY YEAR (sfdat) DESC, MONTH(sfdat) DESC");

        $query->bindParam(':date', $date);

        $query->execute();

        return $query->fetchAll(\PDO::FETCH_OBJ);

    }
}