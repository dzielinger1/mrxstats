<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16/01/2018
 * Time: 15:49
 */

namespace App\Repository;

class RetourRepository extends Repository
{

    public function getNombreDefaut($dateDebut, $dateFin, $mat1,$mat2,$mat3,$mat4)
    {

        $query = $this->db->prepare("SELECT RFDEFAUT,COUNT(RFNUMCMD) AS Nbdefaut
 FROM XMRFIC.RETFABP1 
 WHERE RFDATEDF>= :dateDebut
 AND RFDATEDF<= :dateFin
 AND (RFMATCMD= :mat1
 OR RFMATCMD= :mat2
 OR RFMATCMD= :mat3
 OR RFMATCMD= :mat4) 
 GROUP BY RFDEFAUT");


        $query->bindParam(':dateDebut', $dateDebut);
        $query->bindParam(':dateFin', $dateFin);
        $query->bindParam(':mat1', $mat1);
        $query->bindParam(':mat2', $mat2);
        $query->bindParam(':mat3', $mat3);
        $query->bindParam(':mat4', $mat4);

        $query->execute();

        $result = $query->fetchAll(\PDO::FETCH_OBJ);

        return $result;
    }

    public function getDetailDefaut($dateDebut, $dateFin, $defaut) {

        $tabPourLeExecute = [
            ':dateDebut' => $dateDebut,
            ':dateFin' => $dateFin
        ];
        $inPart = '';
        for ($i = 0; $i < count($defaut); $i++) {
            $inPart .= (($i>0)?', ':'').':inPart'.$i;
            $tabPourLeExecute[':inPart'.$i] = $defaut[$i];
        }

        dump($inPart,$tabPourLeExecute);

        $query = $this->db->prepare('SELECT RFNUMCMD, RFNUMLIG, RFNUMEDT,RFVERDOG,RFDEFAUT,RFREMARK,RFDATEDF,RFHEURDF,CMD.ARLAR AS LABELCMD,
CONCAT(RFSSPCMD,RFSPHCMD),CONCAT(RFSCYCMD,RFCYLCMD),RFAXECMD,RFADDCMD,RFSP1CMD,RFSP2CMD,RFSP3CMD,RFSP4CMD,RFSP5CMD,
RFPXUCMD,RFBASSFI,RFADDSFI,SF.ARLAR AS LABELSF,RFFOUSFI,RFPXUSFI,RFORIGIN,RFDEUTIL,RFNUMGLA,RFNUMGEN,RFNUMPOL,RFNUMTRT,RFNUMVER,
RFPOLOUT,RFPOLTYP,RFAPOLIR,RFADOUCI,RFCTRLE1,RFCTRLE2,RFCTRLE3,RFCYCTRT,RFCYCVER,RFDATGEN,RFSORSTO          
FROM XMRFIC.RETFABP1
INNER JOIN MRFIC2.ARTI01 AS CMD
ON (RFMATCMD = CMD.ARMAT AND  RFGENCMD = CMD.ARGEN AND RFTYPCMD = CMD.ARTYP AND RFINDCMD = CMD.ARIND AND RFTEICMD = CMD.ARTEI AND RFDIACMD = CMD.ARDIA )
INNER JOIN MRFIC2.ARTI01 AS SF
ON (RFMATSFI = SF.ARMAT AND  RFGENSFI = SF.ARGEN AND RFTYPSFI = SF.ARTYP AND RFINDSFI = SF.ARIND AND RFTEISFI = SF.ARTEI AND RFDIASFI = SF.ARDIA )
WHERE RFDATEDF>= :dateDebut
AND RFDATEDF<= :dateFin
AND RFDEFAUT in ('.$inPart.')');

        $query->execute($tabPourLeExecute);

        $result = $query->fetchAll(\PDO::FETCH_OBJ);

        return $result;
    }
}