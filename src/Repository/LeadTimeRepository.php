<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16/01/2018
 * Time: 14:11
 */

namespace App\Repository;

class LeadTimeRepository extends Repository
{
    public function getLeadtimeDaily()
    {
        $query = $this->db->prepare("select (SEANN * 100 + SEMOI) * 100 +SEJOU AS SEDATE,
 sefab,
 sesof,
 sed00,
 sed01,
 sed02,
 sed03,
 sed04,
 sed05,
 sed06,
 sed07,
 sed08,
 sed09,
 sed10,
 sed99,
 case sesof 
  when 'F' then sed00+sed01+sed02+sed03  --leadtime fab quantite cumulee d+3
   when 'S' then sed00+sed01 --leadtime stock quantite cumulee d+1
 end as seldt
from stats.STATEXP10
where seann >= year(now())-1 and (sesof = 'F' OR sesof = 'S')
ORDER BY (SEANN * 100 + SEMOI) * 100 +SEJOU DESC");

        $query->execute();
        return $query->fetchAll(\PDO::FETCH_OBJ);

    }
    public function getLeadtimeMonthly()
    {
        $query =  $this->db->prepare("select (SEANN * 100 + SEMOI) AS SEDATE,
sesof,
case sesof 
  when 'F' then sum(sed00 +sed01 + sed02+ sed03)			  --leadtime fab quantite cumulee d+3
  when 'S' then sum(sed00+sed01) 				--leadtime stock quantite cumulee d+1
 end as seldt,
 SUM(sed00) as J0,
 SUM(sed01) as J1,
 SUM(sed02) as J2,
 SUM(sed03) as J3,
 SUM(sed04) as J4,
 SUM(sed05) as J5,
 SUM(sed06) as J6,
 SUM(sed07) as J7,
 SUM(sed08) as J8,
 SUM(sed09) as J9,
 SUM(sed10) as J10,
 SUM(sed99) as J99
from stats.STATEXP10
where seann >= year(now())-1 and (sesof = 'F' or sesof = 'S')
GROUP BY (SEANN * 100 + SEMOI), sesof
ORDER BY SEANN * 100 + SEMOI DESC");

        $query->execute();
        return $query->fetchAll(\PDO::FETCH_OBJ);
    }
}