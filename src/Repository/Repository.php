<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 18/01/2018
 * Time: 14:23
 */

namespace App\Repository;

class Repository
{
    public $db;

    public function __construct(\App\Service\DBConnect $db)
    {
        $this->db = $db;
    }
}