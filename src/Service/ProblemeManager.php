<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 05/06/2018
 * Time: 09:34
 */

namespace App\Service;


use App\Entity\Actions;
use Doctrine\ORM\EntityManager;

class Probleme
{

    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function detailProbleme($id) {

        $actions = $this->entityManager->getRepository(Actions::class)->findBy(['probleme' => $id]);
        $probleme = $this->entityManager->getRepository(Probleme::class)->findOneBy(['id' => $id]);

        $actionTotal = count($actions);
        $actionsPending = 0;
        $actionsCancelled = 0;
        $actionsTerminated = 0;

        for ($i = 0; ($i < $actionTotal); $i++) {

            if ($actions[$i]->getStatus() == 0) {
                $actionsPending++;
            }
            if ($actions[$i]->getStatus() == 1) {
                $actionsTerminated ++;
            }
            if ($actions[$i]->getStatus() == 2) {
                $actionsCancelled ++;
            }
        }
        if ($actionsCancelled + $actionsTerminated + $actionsPending != 0) {
            $probleme->setAvancement(round((($actionTotal - $actionsPending) * 100) / ($actionsCancelled + $actionsTerminated + $actionsPending)));
        }

        $this->entityManager->persist($probleme);
        $this->entityManager->flush();
    }

}