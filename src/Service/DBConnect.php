<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 17/01/2018
 * Time: 16:32
 */

namespace App\Service;

class DBConnect
{
private $connection;

    public function __construct($dbHost, $dbUser, $dbPass) {

        $this->connection = new \PDO('odbc:DRIVER={Client Access ODBC Driver (32-bit)};System=' . $dbHost, $dbUser, $dbPass);
    }

    public function prepare($statement, array $driver_options = array()) {

        return $this->connection->prepare($statement, $driver_options);

    }

}