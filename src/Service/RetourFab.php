<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 07/03/2018
 * Time: 09:48
 */

namespace App\Service;

use App\Repository\ParamsRepository;
use App\Repository\RetourRepository;
use Symfony\Component\HttpFoundation\Request;


class RetourTab
{

    protected $retourRepository;

    protected $paramsRepository;

    public function __construct(
        RetourRepository $retourRepository,
        ParamsRepository $paramsRepository
)
    {
        $this->retourRepository = $retourRepository;
        $this->paramsRepository = $paramsRepository;
    }

    public function retourTab(Request $request) {

        $dateDebut = $request->get('startDate');
        $dateFin = $request->get('endDate');

        $ReqCodeDefaut = $this->paramsRepository->getCodeDefaut();
        $max2 = (count($ReqCodeDefaut));
        for ($i = 0 ; $i < $max2  ; $i++) {
            $defaut[] = $ReqCodeDefaut[$i][0];
        }

        if($request->get('defaut')) {
            $defaut = (is_array($request->get('defaut')))?$request->get('defaut'):[$request->get('defaut')];
        }

        $detailDefaut = $this->retourRepository->getDetailDefaut($dateDebut, $dateFin, $defaut);

        $max = (count($detailDefaut));
        for($i = 0 ; $i < $max  ; $i++) {
            $detailDefaut[$i] = (array) $detailDefaut[$i];
            $detailDefaut[$i]  = array_map("utf8_encode", $detailDefaut[$i] );
        }
        $detailDefaut = array_map('array_values', $detailDefaut);

        return $detailDefaut;
    }
}