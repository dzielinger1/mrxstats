<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2018
 * Time: 11:30
 */

namespace App\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;

// Types
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

// Validators
use Symfony\Component\Validator\Constraints\NotBlank;


class NouveauProjetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateProbleme', DateType::class, [
                'constraints' => [
                    new NotBlank(array(
                        'message' => 'Veuillez renseigner la date du problème',
                    )),
                ],
                'label' => 'Date du Problème',
            ])
            ->add('probleme', TextType::class, [
                'constraints' => [
                    new NotBlank(array(
                        'message' => 'Veuillez renseigner votre problème',
                    )),
                ],
                'label' => 'Intitulé',
            ])
            ->add('actions', CollectionType::class, [
                'entry_type' => ActionsType::class,
                'allow_add' => true
            ])
            ->add('responsable', TextType::class, [
                'constraints' => [
                    new NotBlank(array(
                        'message' => 'Veuillez renseigner le repsonsable',
                    )),
                ],
                'label' => 'Responsable',
            ]);
    }
}