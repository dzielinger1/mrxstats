<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 18/12/2017
 * Time: 15:44
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends controller
{
    /**
     * @Route("/")
     * @return Response
     *
     */
    public function indexAction () {

        $reqLabelDefaut = $this->get('app.repository.params_repository')->getCodeDefaut();

        $reqCodeArticle = $this->get('app.repository.params_repository')->getCodeArticle();

        $this->leadtimeDaily();
        $this->leadtimeMonthly();
        return $this->render('pages/index.html.twig', array("reqVolumeDaily" =>$this->volumeDaily(),"reqVolumeMonthly" => $this->volumeMonthly(),"reqLabelDefaut" => $reqLabelDefaut,"reqCodeArticle" => $reqCodeArticle));
    }

    public function volumePosteMonthly ()
    {
        $laserSum = 0;
        $blocageSum = 0;
        $controle1Sum = 0;
        $colorationSum = 0;
        $vernisSum = 0;
        $antirefletSum = 0;
        $detourageSum = 0;
        $controle2Sum = 0;

        $reqVolumePosteMontly = $this->get('app.repository.volume_repository')->getVolumePosteMonthly();

        foreach ($reqVolumePosteMontly as $value) {
            switch (utf8_encode(trim($value->SFPST))) {
                case "305 Laser":
                    $laserSum += $value->SFQVE;
                    break;
                case "320 Blocage":
                    $blocageSum += $value->SFQVE;
                    break;
                case "380 Contrôle 1":
                    $controle1Sum += $value->SFQVE;
                    break;
                case "420 Coloration":
                    $colorationSum += $value->SFQVE;
                    break;
                case "438 Vernis":
                    $vernisSum += $value->SFQVE;
                    break;
                case "455 Antireflet":
                    $antirefletSum += $value->SFQVE;
                    break;
                case "480 Détourage":
                    $detourageSum += $value->SFQVE;
                    break;
                case "800 Contrôle 2":
                    $controle2Sum += $value->SFQVE;
                    break;
            }
        }
    }

    public function volumeDaily () {

        $dt = new \DateTime("-1 month");
        $date = $dt->format("Y-m-d");

        $reqVolumeDaily = $this->get('app.repository.volume_repository')->getVolumeDaily($date);

        $value = "6";
        //$pattern = '/^[-+]?([0-9]{1,2})([.,][0-9]+)?$/';
        //$pattern = '/^\d*\.?\d*((25)|(50)|(5)|(75)|(0)|(00))?$/';
        //dump(preg_match($pattern, $subject));

        if (fmod($value,0.25) != 0) {

            dump("Erreur");
        }

        return $reqVolumeDaily;
    }

    public function volumeMonthly ()
    {
        $dt = new \DateTime("-1 year");
        $date = $dt->format("Y-m-01");

        $reqVolumeMonthly= $this->get('app.repository.volume_repository')->getVolumeMonthly($date);

        return $reqVolumeMonthly;
    }

    public function leadtimeDaily () {

        $reqLeadTimeDaily= $this->get('app.repository.lead_time_repository')->getLeadtimeDaily();

        foreach ($reqLeadTimeDaily as $value) {

            $leadTimeDaily[] = $objectLeadTime =  (object) ['date' => ($value->SEDATE),'pourcentage' => round (($value->SELDT * 100 ) / ($value->SED00+$value->SED01+$value->SED02+$value->SED03+$value->SED04+$value->SED05+$value->SED06+$value->SED07+$value->SED08+$value->SED09+$value->SED10+$value->SED99)), 'Type' => $value->SESOF];
        }

        return $reqLeadTimeDaily;
    }

    public function leadtimeMonthly () {

        $reqLeadTimeMonthly= $this->get('app.repository.lead_time_repository')->getLeadtimeMonthly();

        foreach ($reqLeadTimeMonthly as $value) {
            $leadTimeMonthly[] = $objectLeadTime =  (object) ['date' => ($value->SEDATE),'pourcentage' => round (($value->SELDT * 100 ) / ($value->J0+$value->J1+$value->J2+$value->J3+$value->J4+$value->J5+$value->J6+$value->J7+$value->J8+$value->J9+$value->J10+$value->J99)), 'Type' => $value->SESOF];
        }

        return $reqLeadTimeMonthly ;
    }
}