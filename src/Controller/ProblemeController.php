<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16/05/2018
 * Time: 09:45
 */

namespace App\Controller;


use App\Entity\Actions;
use App\Entity\Probleme;
use App\Form\Type\ActionsType;
use App\Form\Type\DeleteProblemeType;
use App\Form\Type\ManageActionType;
use App\Service\ProblemeManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Type\NouveauProblemeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class AdminController extends Controller
{

    /**
     * @Route("/volumefabprojetadd", name = "volumefabprojetadd")
     */

    public function addProjetAction(Request $request) {

        $newProbleme= new Probleme();
        $nouveauProjetForm = $this->createForm(NouveauProblemeType::class, $newProbleme);
        $nouveauProjetForm->handleRequest($request);
        $categorie = $request->query->get('categorie');
        $em = $this->getDoctrine()->getManager();

        if ($nouveauProjetForm->isSubmitted() && $nouveauProjetForm->isValid()) {
            $newProbleme->setCategorie($categorie);
            $em->persist($newProbleme);
            $em->flush();
            switch ($categorie) {
                case 0 :
                    return $this->redirectToRoute('volumefabprobleme');
                    break;
                case 1 :
                    return $this->redirectToRoute('leadtimefabprobleme');
                    break;
            }
        }
        return $this->render('pages/problemeNew.html.twig', array('nouveauProjetType' => $nouveauProjetForm->createView()));
    }

    /**
     * @Route("/problemedelete/{id}", name = "problemedelete")
     */

    public function deleteProblemeAction(Probleme $probleme)
    {
        $categorie = $probleme->getCategorie();
        $em = $this->getDoctrine()->getManager();
        $em->remove($probleme);
        $em->flush();

        switch ($categorie) {
            case 0 :
                return $this->redirectToRoute('volumefabprobleme');
                break;
            case 1 :
                return $this->redirectToRoute('leadtimefabprobleme');
                break;
        }
    }

    /**
     * @Route("/problemedetail/{id}", name = "problemedetail")
     */

    public function detailProblemeAction(Probleme $probleme, Request $request, ProblemeManager $problemeManager)
    {
        $id = $probleme->getId();
        $newActions = new Actions();
        $newActionsForm = $this->createForm(ActionsType::class,$newActions);
        $newActionsForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        $actions =  $em->getRepository(Actions::class)->findBy(['probleme' => $id]);

        if ($newActionsForm->isSubmitted() && $newActionsForm->isValid()) {

            $newActions->setProbleme($probleme);
            $em->persist($newActions);
            $em->flush();
            return $this->redirectToRoute('problemedetail',array('id' => $id));
        }

        $problemeManager->detailProbleme($probleme,$actions);

        $em->persist($probleme);

        return $this->render('/pages/problemeDetail.html.twig', ['newActionsType'=>$newActionsForm->createView(),'probleme' => $probleme, 'actions' => $actions]);
    }

    /**
     * @Route("/volumefabactionmanage/{id}/{idprobleme}/{status}", name = "volumefabactionmanage")
     * @ParamConverter("actions", class="App\Entity\Actions", options={
     * "mapping": {"id": "id"}})
     * @ParamConverter("probleme", class="App\Entity\Probleme", options={"mapping":{"idprobleme": "id"}})
     */

    public function manageActionAction(Actions $actions, Probleme $probleme, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $status = $request->attributes->get('status');

        $actions->setStatus($status);

        $em->persist($actions);
        $em->flush();

        return $this->redirectToRoute('problemedetail',array('id' => $probleme->getId()));
    }

}