<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 13/02/2018
 * Time: 14:16
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends controller
{

    /**
     * @Route("/api/nombredefaut")
     * @return Response
     *
     */
    public function getNombreDefautAction(Request $request)
    {
        $dateDebut = $request->get('startDate');
        $dateFin = $request->get('endDate');

        $mat1 = "120";
        $mat2 = "150";
        $mat3 = "140";
        $mat4 = "170";
        $nombreDefaut = $this->get('app.repository.retour_repository')->getNombreDefaut($dateDebut, $dateFin, $mat1, $mat2, $mat3, $mat4);
        $json = new JsonResponse();
        $json->setData($nombreDefaut);
        dump($json);
        return $json;
    }

    /**
     * @Route("/api/detaildefaut")
     * @return Response
     *
     */
    public function getDetailDefautAction(Request $request)
    {
        $detailDefaut = $this->get('app.retourtab')->RetourTab($request);

        $json = new JsonResponse($detailDefaut);

        dump($json);

        return $json;
    }

}